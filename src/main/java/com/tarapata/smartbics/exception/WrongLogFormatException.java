package com.tarapata.smartbics.exception;

public class WrongLogFormatException extends ParsingException {
    public WrongLogFormatException(String message) {
        super(message);
    }
}
