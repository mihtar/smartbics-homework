package com.tarapata.smartbics.exception;

public class ParsingException extends Exception {
    public ParsingException(String message) {
        super(message);
    }
}
