package com.tarapata.smartbics.logparser;

import com.tarapata.smartbics.exception.ParsingException;
import com.tarapata.smartbics.model.LogEntry;
import com.tarapata.smartbics.model.LogLevel;
import com.tarapata.smartbics.exception.WrongLogFormatException;

import java.time.LocalDateTime;

public class SplittingLogParser implements LogParser {
    @Override
    public LogEntry parse(String logLine) throws ParsingException {
        String[] splittedLog = logLine.split(";");
        if (splittedLog.length != 3) {
            throw new WrongLogFormatException("Log line can't be parsed by splitting");
        }
        return LogEntry.builder()
                       .date(LocalDateTime.parse(splittedLog[0]))
                       .level(LogLevel.create(splittedLog[1]))
                       .message(splittedLog[2])
                       .build();
    }
}
