package com.tarapata.smartbics.logparser;

import com.tarapata.smartbics.model.LogEntry;
import com.tarapata.smartbics.exception.ParsingException;

public interface LogParser {
    LogEntry parse(String logLine) throws ParsingException;
}
