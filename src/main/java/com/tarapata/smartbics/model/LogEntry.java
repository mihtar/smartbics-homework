package com.tarapata.smartbics.model;

import lombok.*;

import java.time.LocalDateTime;

import static com.tarapata.smartbics.utils.Utils.getOrEmptyString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LogEntry {
    private LocalDateTime date;
    private LogLevel level;
    private String message;

    @Override
    public String toString() {
        return String.format("%s;%s;%s", getOrEmptyString(date), level, getOrEmptyString(message));
    }


}
