package com.tarapata.smartbics.classifier;

import com.tarapata.smartbics.model.LogEntry;

import java.util.function.Function;

@FunctionalInterface
public interface LogClassifier<T> {
    Function<LogEntry, T> classify();
}
